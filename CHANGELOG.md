
# CHANGELOG

This CHANGELOG is a format conforming to [keep-a-changelog](https://github.com/olivierlacan/keep-a-changelog). 
It is generated with git-chglog -o CHANGELOG.md


<a name="v0.3.1"></a>
## [v0.3.1](https://gitlab.com/cestus/tools/fabricator/compare/v0.3.0...v0.3.1)

### Chore

* dependency updates


<a name="v0.3.0"></a>
## [v0.3.0](https://gitlab.com/cestus/tools/fabricator/compare/v0.2.1...v0.3.0)

### Feat

* add NewGinkoTestIOStreams which flush to GinkoWriter


<a name="v0.2.1"></a>
## [v0.2.1](https://gitlab.com/cestus/tools/fabricator/compare/v0.2.0...v0.2.1)

### Fix

* Release script


<a name="v0.2.0"></a>
## [v0.2.0](https://gitlab.com/cestus/tools/fabricator/compare/v0.1.2...v0.2.0)

### Feat

* Use code.cestus.io/libs/buildinfo instead of own implementation


<a name="v0.1.2"></a>
## [v0.1.2](https://gitlab.com/cestus/tools/fabricator/compare/v0.1.1...v0.1.2)

### Fix

* version.yaml


<a name="v0.1.1"></a>
## [v0.1.1](https://gitlab.com/cestus/tools/fabricator/compare/v0.1.0...v0.1.1)

### Fix

* Version when installed with go install


<a name="v0.1.0"></a>
## v0.1.0

### Feat

* Add fabricator cli base

